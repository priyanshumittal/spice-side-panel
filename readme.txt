=== Spice Side Panel ===

Contributors:           spicethemes
Tags:                   slide panel, side panel, slide sidebar, sliding widget, panel slider
Requires at least:      5.3
Requires PHP:           5.2
Tested up to:           6.7.1
Stable tag:             1.0.1
License:                GPLv2 or later
License URI:            https://www.gnu.org/licenses/gpl-2.0.html

== Description ==

This plugin allows to add the side panel in your site in which you can add your favorite widgets. It is very flexible, fast, and smooth.

<h3>Key Features</h3>
* Side Panel with Slide
* Panel Custom Width
* Show panel alignment to the left or right
* Overlay setting if panel is opened
* Typography setting
* Color setting


== Changelog ==

@Version 1.0.1
* Updated freemius directory and fixed the warning issue.

@Version 1.0
* Updated Freemius Code.

@Version 0.2.1
* Updated Freemius Code.

@Version 0.1
* Initial release.

======= External Resources =======

Font Awesome
Copyright: (c) Dave Gandy
License: https://fontawesome.com/license ( Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License )
Source: https://fontawesome.com

Alpha color picker Control:
Copyright: (c) 2016 Codeinwp cristian-ungureanu
License: MIT License
Source: https://github.com/Codeinwp/customizer-controls/tree/master/customizer-alpha-color-picker

Customizer Toggle Control
Copyright: (c) 2016 soderlind
License: Under GNU General Public License v2.0
Source: https://github.com/soderlind/class-customizer-toggle-control

Custom Slider & Text Radio Button Controls
Copyright: (c) Anthony Hortin
License: Under GNU General Public License v2.0
Source: https://github.com/maddisondesigns/customizer-custom-controls