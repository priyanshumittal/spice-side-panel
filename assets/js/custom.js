jQuery(document).ready(function() {
  
if(jQuery( "body" ).hasClass( "spice-panel-alignment-left" ))
{
jQuery('.ssp-circle, .sp-panel-close').click(function() {
    let spWidth = jQuery('#spice_side_panel_id').val();
     let spMarginLeft = parseInt(jQuery('.spice-side-panel').css('margin-left'),10);
     let w = (spMarginLeft >= 0 ) ? spWidth * -1 : 0;
      let cw = (w < 0) ? -w : spWidth-22;

  jQuery('.spice-side-panel').animate({
    marginLeft:w
  });
 jQuery('.spice-side-panel span').animate({
    marginLeft:w
  });
  jQuery('.ssp-circle').animate({
    left:cw
  },function() {
     jQuery('.fa-chevron-left').toggleClass('hide');
    jQuery('.fa-chevron-right').toggleClass('hide');
    jQuery('.side-panel-overlay').toggleClass('show');
  });
});
}
else
{
   jQuery('.ssp-circle, .sp-panel-close').click(function() {
    let spWidth = jQuery('#spice_side_panel_id').val();
     let spMarginLeft = parseInt(jQuery('.spice-side-panel').css('margin-right'),10);
     let w = (spMarginLeft >= 0 ) ? spWidth * -1 : 0;
      let cw = (w < 0) ? -w : spWidth-22;

  jQuery('.spice-side-panel').animate({
    marginRight:w
  });
 jQuery('.spice-side-panel span').animate({
    marginRight:w
  });
  jQuery('.ssp-circle').animate({
    right:cw
  },function() {
     jQuery('.fa-chevron-left').toggleClass('hide');
    jQuery('.fa-chevron-right').toggleClass('hide');
    jQuery('.side-panel-overlay').toggleClass('show');
  });
});
}
});