<?php
/*
* Plugin Name:			Spice Side Panel
* Plugin URI:
* Description:			This plugin allows to add the side panel in your site in which you can add your favorite widgets. It is very flexible, fast, and smooth.
* Version:				1.0.1
* Requires at least:	5.3
* Requires PHP:			5.2
* Tested up to:			6.7.1
* Author:				Spicethemes
* Author URI:			https://spicethemes.com
* License:				GPLv2 or later
* License URI:			https://www.gnu.org/licenses/gpl-2.0.html
* Text Domain:			spice-side-panel
* Domain Path:			/languages
*/

//Freemius SDK Snippet
if ( ! function_exists( 'ssp_fs' ) ) {
    // Create a helper function for easy SDK access.
    function ssp_fs() {
        global $ssp_fs;

        if ( ! isset( $ssp_fs ) ) {
            // Include Freemius SDK.
            if ( function_exists('olivewp_companion_activate') && defined( 'OWC_PLUGIN_DIR' ) && file_exists(OWC_PLUGIN_DIR . '/inc/freemius/start.php') ) {
                // Try to load SDK from olivewp companion folder.
                require_once OWC_PLUGIN_DIR . '/inc/freemius/start.php';
            } else if ( function_exists('olivewp_plus_activate') && defined( 'OLIVEWP_PLUGIN_DIR' ) && file_exists(OLIVEWP_PLUGIN_DIR . '/freemius/start.php') ) {
                // Try to load SDK from premium olivewp companion plugin folder.
                require_once OLIVEWP_PLUGIN_DIR . '/freemius/start.php';
            } else {
                require_once dirname(__FILE__) . '/freemius/start.php';
            }

            $ssp_fs = fs_dynamic_init( array(
                'id'                  => '10577',
                'slug'                => 'spice-side-panel',
                'premium_slug'        => 'spice-side-panel',
                'type'                => 'plugin',
                'public_key'          => 'pk_ff197f154e1830cbcdf26314f2af3',
                'is_premium'          => true,
                'is_premium_only'     => true,
                'has_paid_plans'      => true,
                'is_org_compliant'    => false,
                'menu'                => array(
                    'support'        => false,
                ),
                // Set the SDK to work in a sandbox mode (for development & testing).
                // IMPORTANT: MAKE SURE TO REMOVE SECRET KEY BEFORE DEPLOYMENT.
                ) );
        }

        return $ssp_fs;
    }
	ssp_fs();
}

// Exit if accessed directly
if( ! defined('ABSPATH'))
{
	die('Do not open this file directly.');
}

/**
 * Main Spice_Side_Panel Class
 *
 * @class Spice_Side_Panel
 * @since 0.1
 * @package Spice_Side_Panel
*/

final class Spice_Side_Panel {

	/**
	 * The version number.
	 *
	 * @var     string
	 * @access  public
	 * @since   0.1
	 */
	public $version;


	/**
	 * Constructor function.
	 *
	 * @access  public
	 * @since   0.1
	 * @return  void
	 */

	private string $plugin_url;
    private string $plugin_path;

	public function __construct()
	{
		$this->plugin_url  = plugin_dir_url( __FILE__ );
		$this->plugin_path = plugin_dir_path( __FILE__ );
		$this->version     = '0.1';

		define( 'SPICE_SIDE_PANEL_URL', $this->plugin_url );
		define( 'SPICE_SIDE_PANEL_PATH', $this->plugin_path );
		define( 'SPICE_SIDE_PANEL_VERSION', $this->version );

		add_action( 'widgets_init', array( $this, 'side_panel_widgets' ) );
		add_action( 'customize_register', array( $this, 'side_panel_controls' ) );
		add_action( 'after_setup_theme' , array( $this, 'side_panel_register_options' ) );
		add_action( 'admin_enqueue_scripts', array( $this,'side_pane_admin_script' ));
		add_action( 'wp_enqueue_scripts', array( $this, 'side_panel_enqueue_scripts' ));
		add_filter( 'body_class', array( $this, 'side_panel_body_class' ) );
		add_action( 'wp_footer', array( $this, 'side_panel_content' ) );
		add_action( 'init', array( $this, 'load_plugin_textdomain' ) );
	}


	/**
	* Register widget Area
	*/
	public function side_panel_widgets()
	{
		register_sidebar( array(
			'name'          => esc_html__( 'Side Panel Sidebar','spice-side-panel' ),
			'id'            => 'spice_panel_sidebar',
			'description'   => esc_html__( 'Add Sidebar widgets.', 'spice-side-panel' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );
	}


	/**
	* Adds custom controls
	*/
	public function side_panel_controls( $wp_customize )
	{
		require_once ( SPICE_SIDE_PANEL_PATH . 'inc/customizer/controls/toggle/class-toggle-control.php' );
		require_once ( SPICE_SIDE_PANEL_PATH . 'inc/customizer/controls/customizer-text-radio/custom-controls.php' );
		require_once ( SPICE_SIDE_PANEL_PATH . 'inc/customizer/controls/range/range-control.php' );
		require_once ( SPICE_SIDE_PANEL_PATH . 'inc/customizer/controls/color/color-control.php' );
		$wp_customize->register_control_type('Spice_Side_Panel_Toggle_Control');
	}



	/**
	* Adds customizer options
	*/
	public function side_panel_register_options()
	{
		require_once ( SPICE_SIDE_PANEL_PATH . 'inc/customizer/sanitization.php' );
		require_once ( SPICE_SIDE_PANEL_PATH . 'inc/customizer/customizer.php' );
		require_once ( SPICE_SIDE_PANEL_PATH . 'inc/customizer/fonts.php' );
	}



	/**
	* Load admin style
	*/
	public function side_pane_admin_script()
	{
		wp_enqueue_style('spice-side-panel-admin', SPICE_SIDE_PANEL_URL .'assets/css/admin.css');
	}


	/**
	* Load script & style
	*/
	public function side_panel_enqueue_scripts()
	{
		wp_enqueue_style('spice-side-panel-font-awesome', SPICE_SIDE_PANEL_URL . 'assets/css/font-awesome/css/all.min.css');
		wp_enqueue_style('spice-side-panel-style', SPICE_SIDE_PANEL_URL . 'assets/css/style.css');	
		wp_enqueue_script('spice-side-panel-custom', SPICE_SIDE_PANEL_URL . 'assets/js/custom.js', array('jquery'));	
	}



	/**
	* Add body class
	*/
	function side_panel_body_class( $classes ) 
	{
    $classes[] = 'spice-panel-alignment-'.get_theme_mod('spice_side_panel_alignment','right');
    return $classes;
	}


	
	/**
	* Panel Code
	*/
	public function side_panel_content() {
		require_once ( SPICE_SIDE_PANEL_PATH . 'inc/view.php' );
 	}

 	/**
	 * Load the localisation file.
	 *
	 * @access  public
	 * @since   0.1
	 * @return  void
	 */
	public function load_plugin_textdomain() {
		load_plugin_textdomain( 'spice-side-panel' , false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
	}



}

new Spice_Side_Panel;