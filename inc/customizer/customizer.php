<?php
// Adding customizer settings
function spice_side_panel_customizer_controls( $wp_customize )
{
   $spice_side_panel_font_size = array();
    for($i=10; $i<=100; $i++) {
      $spice_side_panel_font_size[$i] = $i;
    }
    $spice_side_panel_line_height = array();
    for($i=0; $i<=100; $i++) {
        $spice_side_panel_line_height[$i] = $i;
    }
    $spice_side_panel_font_style = array('normal'=>'Normal','italic'=>'Italic');
    $spice_side_panel_text_transform = array('default'=>'Default','capitalize'=>'Capitalize','lowercase'=>'Lowercase','Uppercase'=>'Uppercase');
    $spice_side_panel_font_weight = array('100'=>'100','200'=>'200','300'=>'300','400'=>'400','500'=>'500','600'=>'600','700'=>'700','800'=>'800','900'=>'900');
    $spice_side_panel_font_family = spice_side_panel_typo_fonts();
    

    /* ===================================================================================================================
    * Spice Side Panel *
    * ====================================================================================================================== */
    
    $wp_customize->add_panel('spice_side_panel',
        array(
            'priority'   => 155,
            'capability' => 'edit_theme_options',
            'title'      => esc_html__('Spice Side Panel','spice-side-panel')
        )
    );

    
    /* ===================================================================================================================
    * Spice Side Panel General Setting *
    * ====================================================================================================================== */
    
    $wp_customize->add_section( 'spice_side_panel_customizer' , array(
        'title'    => esc_html__('General Settings', 'spice-side-panel'),
        'priority' => 1,
        'panel'    => 'spice_side_panel',
        )
    );


    //Position Left/ Right
    $wp_customize->add_setting( 'spice_side_panel_alignment',
        array(
        'default'           => 'right',
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'spice_side_panel_sanitize_text'
        )
    );
    $wp_customize->add_control( new Spice_Side_Panel_Text_Radio_Control( $wp_customize, 'spice_side_panel_alignment',
        array(
        'label'   =>  esc_html__( 'Panel Position', 'spice-side-panel' ),
        'section' =>  'spice_side_panel_customizer',
        'priority' =>  1,
        'choices' => array(
            'left' => esc_html__( 'Left'  , 'spice-side-panel'),
            'right' => esc_html__( 'Right', 'spice-side-panel')
                )
            )
     ) );


    //Position From Top
    $wp_customize->add_setting('spice_side_panel_top_position',
        array(
            'default'           =>  23,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Spice_Side_Panel_Slider_Control( $wp_customize, 'spice_side_panel_top_position',
        array(
            'label'             =>  esc_html__( 'Icon Position From Top ( In % )', 'spice-side-panel' ),
            'section'           =>  'spice_side_panel_customizer',
            'setting'           =>  'spice_side_panel_top_position',
            'priority'          =>   2,
            'input_attrs'   => array(
                'min'   => 0,
                'max'   => 95,
                'step'  => 1
            ),
        )
    ));


    // Panel Heading
    $wp_customize->add_setting('spice_side_panel_title',
        array(
            'default'           =>  esc_html__('Close', 'spice-side-panel' ),
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_side_panel_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_side_panel_title', 
        array(
            'label'    =>  esc_html__('Panel Heading','spice-side-panel' ),
            'section'  =>  'spice_side_panel_customizer',
            'setting'  =>  'spice_side_panel_title',
            'type'     =>  'text',
            'priority' =>  3,
        )
    );


    // Panel Width
    $wp_customize->add_setting('spice_side_panel_break_point',
        array(
            'default'           =>  320,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Spice_Side_Panel_Slider_Control( $wp_customize, 'spice_side_panel_break_point',
        array(
            'label'             =>  esc_html__( 'Break Point', 'spice-side-panel' ),
            'section'           =>  'spice_side_panel_customizer',
            'setting'           =>  'spice_side_panel_break_point',
            'priority'          =>  4,
            'input_attrs'   => array(
                'min'   => 1,
                'max'   => 2000,
                'step'  => 1
            ),
        )
    ));

    // Panel Width
    $wp_customize->add_setting('spice_side_panel_width',
        array(
            'default'           =>  320,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Spice_Side_Panel_Slider_Control( $wp_customize, 'spice_side_panel_width',
        array(
            'label'             =>  esc_html__( 'Panel Width (Px)', 'spice-side-panel' ),
            'section'           =>  'spice_side_panel_customizer',
            'setting'           =>  'spice_side_panel_width',
            'priority'          =>  4,
            'input_attrs'   => array(
                'min'   => 100,
                'max'   => 700,
                'step'  => 5
            ),
        )
    ));



    Class Spice_Side_Panel_Padding_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Padding', 'spice-side-panel' ); ?></h3>
        <?php }
    }

    $wp_customize->add_setting('spice_panel_padding',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field'
        )
    );
    $wp_customize->add_control(new Spice_Side_Panel_Padding_Customize_Control($wp_customize, 'spice_panel_padding', 
        array(
                'section'           =>  'spice_side_panel_customizer',
                'setting'           =>  'spice_panel_padding'
            )
        )
    );


    // Panel Top Padding
    $wp_customize->add_setting( 'spice_side_panel_top',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_side_panel_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'spice_side_panel_top',
        array(
            'label'       => esc_html__( 'Top', 'spice-side-panel' ),
            'section'     => 'spice_side_panel_customizer',
            'type'        => 'number',
            'input_attrs' => array( 'min' => 0, 'max' => 200, 'step' => 1, 'style' => 'width: 70px;' ),
        )
    );


    // Panel Right Padding
    $wp_customize->add_setting( 'spice_side_panel_right',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_side_panel_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'spice_side_panel_right',
        array(
            'label'       => esc_html__( 'Right', 'spice-side-panel' ),
            'section'     => 'spice_side_panel_customizer',
            'type'        => 'number',
            'input_attrs' => array( 'min' => 0, 'max' => 200, 'step' => 1, 'style' => 'width: 70px;' ),
        )
    );


    // Panel Bottom Padding
    $wp_customize->add_setting( 'spice_side_panel_bottom',
        array(
            'default'           => 10,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_side_panel_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'spice_side_panel_bottom',
        array(
            'label'       => esc_html__( 'Bottom', 'spice-side-panel' ),
            'section'     => 'spice_side_panel_customizer',
            'type'        => 'number',
            'input_attrs' => array( 'min' => 0, 'max' => 200, 'step' => 1, 'style' => 'width: 70px;' ),
        )
    );


    // Panel Left Padding
    $wp_customize->add_setting( 'spice_side_panel_left',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_side_panel_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'spice_side_panel_left',
        array(
            'label'       => esc_html__( 'Left', 'spice-side-panel' ),
            'section'     => 'spice_side_panel_customizer',
            'type'        => 'number',
            'input_attrs' => array( 'min' => 0, 'max' => 200, 'step' => 1, 'style' => 'width: 70px;' ),
        )
    );


    /* ===================================================================================================================
    * Side Panel Typography Setting *
    ====================================================================================================================== */
    
    $wp_customize->add_section( 'spice_side_panel_typo_section' , array(
        'title'   => esc_html__('Typography Settings', 'spice-side-panel'),
        'priority'=> 2,
        'panel'   => 'spice_side_panel',
        ) 
    );

    $wp_customize->add_setting('spice_side_panel_typo',
        array(
            'default'           => false,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_side_panel_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Spice_Side_Panel_Toggle_Control( $wp_customize, 'spice_side_panel_typo',
        array(
            'label'     =>  esc_html__( 'Enable to apply the settings', 'spice-side-panel'  ),
            'section'   =>  'spice_side_panel_typo_section',
            'setting'   =>  'spice_side_panel_typo',
            'type'      =>  'toggle'
        )
    ));


    Class Spice_Side_Panel_Heading_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Panel Heading', 'spice-side-panel' ); ?></h3>
        <?php }
    }

    $wp_customize->add_setting('spice_panel_heading',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field'
        )
    );
    $wp_customize->add_control(new Spice_Side_Panel_Heading_Customize_Control($wp_customize, 'spice_panel_heading', 
        array(
                'active_callback'   =>  'spice_side_panel_typo_callback',
                'section'           =>  'spice_side_panel_typo_section',
                'setting'           =>  'spice_panel_heading'
            )
        )
    );


    //Panel Heading Font Family
    $wp_customize->add_setting('spice_side_panel_fontfamily',
        array(
            'default'           =>  'Poppins',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_side_panel_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_side_panel_fontfamily', array(
            'label'           => esc_html__('Font Family','spice-side-panel'),
            'section'         => 'spice_side_panel_typo_section',
            'setting'         => 'spice_side_panel_fontfamily',
            'type'            => 'select',
            'choices'         =>  $spice_side_panel_font_family,
            'active_callback' =>  'spice_side_panel_typo_callback',
    ));


    //Panel Heading Font Size
    $wp_customize->add_setting('spice_side_panel_fontsize',
        array(
            'default'           =>  24,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
            )
    );
    $wp_customize->add_control('spice_side_panel_fontsize', array(
            'label'   => esc_html__('Font Size (px)','spice-side-panel'),
            'section' => 'spice_side_panel_typo_section',
            'setting' => 'spice_side_panel_fontsize',
            'type'    =>  'select',
            'choices' => $spice_side_panel_font_size,
            'active_callback' => 'spice_side_panel_typo_callback',
    ));


    //Panel Heading Line Height
    $wp_customize->add_setting('spice_side_panel_lheight',
        array(
            'default'           =>  60,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control('spice_side_panel_lheight', 
        array(
            'label'   =>  esc_html__('Line Height (px)','spice-side-panel'),
            'section' => 'spice_side_panel_typo_section',
            'setting' => 'spice_side_panel_lheight',
            'type'    => 'select',
            'choices' => $spice_side_panel_line_height,
            'active_callback' => 'spice_side_panel_typo_callback',
        )
    );


    //Panel Heading Font Weight
    $wp_customize->add_setting('spice_side_panel_fontweight',
        array(
            'default'           =>  700,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control('spice_side_panel_fontweight', 
        array(
            'label'   =>  esc_html__('Font Weight','spice-side-panel'),
            'section' => 'spice_side_panel_typo_section',
            'setting' => 'spice_side_panel_fontweight',
            'type'    => 'select',
            'choices' =>  $spice_side_panel_font_weight,
            'active_callback' =>  'spice_side_panel_typo_callback',
        )
    );


    //Panel Heading Font Style
    $wp_customize->add_setting('spice_side_panel_fontstyle',
        array(
            'default'           =>  'normal',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_side_panel_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_side_panel_fontstyle', 
        array(
            'label'   => esc_html__('Font Style','spice-side-panel'),
            'section' => 'spice_side_panel_typo_section',
            'setting' => 'spice_side_panel_fontstyle',
            'type'    => 'select',
            'choices' => $spice_side_panel_font_style,
            'active_callback' =>  'spice_side_panel_typo_callback',
        )
    );


    //Panel Heading Text Transform
    $wp_customize->add_setting('spice_side_panel_transform',
        array(
            'default'           =>  'default',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_side_panel_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_side_panel_transform', 
        array(
            'label'   => esc_html__('Text Transform','spice-side-panel'),
            'section' => 'spice_side_panel_typo_section',
            'setting' => 'spice_side_panel_transform',
            'type'    => 'select',
            'choices'=>  $spice_side_panel_text_transform,
            'active_callback' =>  'spice_side_panel_typo_callback',
        )
    );



    Class Spice_Side_Panel_Title_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Widget Title', 'spice-side-panel' ); ?></h3>  
        <?php }
    }

    $wp_customize->add_setting('spice_panel_title_heading',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field'
        )
    );
    $wp_customize->add_control(new Spice_Side_Panel_Title_Customize_Control($wp_customize, 'spice_panel_title_heading', 
        array(
               'active_callback'    =>  'spice_side_panel_typo_callback',
               'section'            =>  'spice_side_panel_typo_section',
               'setting'            =>  'spice_panel_title_heading'
            )
        )
    );


    //Widget Title Font Family
    $wp_customize->add_setting('spice_side_panel_title_fontfamily',
        array(
        'default'           =>  'Poppins',
        'capability'        =>  'edit_theme_options',
        'sanitize_callback' => 'spice_side_panel_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_side_panel_title_fontfamily', 
        array(
            'label'           =>  esc_html__('Font Family','spice-side-panel'),
            'section'         => 'spice_side_panel_typo_section',
            'setting'         => 'spice_side_panel_title_fontfamily',
            'type'            => 'select',
            'choices'         =>  $spice_side_panel_font_family,
            'active_callback' => 'spice_side_panel_typo_callback',
        )
    );


    //Widget Title Font Size
    $wp_customize->add_setting('spice_side_panel_title_fontsize',
        array(
        'default'           =>  30,
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control('spice_side_panel_title_fontsize', 
        array(
            'label'           => esc_html__('Font Size (px)','spice-side-panel'),
            'section'         => 'spice_side_panel_typo_section',
            'setting'         => 'spice_side_panel_title_fontsize',
            'type'            => 'select',
            'choices'         => $spice_side_panel_font_size,
            'active_callback' => 'spice_side_panel_typo_callback',
        )
    );


    //Widget Title Line Height
    $wp_customize->add_setting('spice_side_panel_title_lheight',
        array(
            'default'           =>   45,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'absint'
        )
    );
    $wp_customize->add_control('spice_side_panel_title_lheight',
        array(
            'label'           =>   esc_html__('Line Height (px)','spice-side-panel'),
            'section'         =>  'spice_side_panel_typo_section',
            'setting'         =>  'spice_side_panel_title_lheight',
            'type'            =>  'select',
            'choices'         =>  $spice_side_panel_line_height,
            'active_callback' =>  'spice_side_panel_typo_callback',
        )
    );


    //Widget Title Font Weight
    $wp_customize->add_setting('spice_side_panel_title_fontweight',
        array(
            'default'           =>  700,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'absint'
        )
    );
    $wp_customize->add_control('spice_side_panel_title_fontweight', 
        array(
            'label'           => esc_html__('Font Weight','spice-side-panel'),
            'section'         => 'spice_side_panel_typo_section',
            'setting'         => 'spice_side_panel_title_fontweight',
            'type'            => 'select',
            'choices'         =>  $spice_side_panel_font_weight,
            'active_callback' =>  'spice_side_panel_typo_callback',
        )
    );


    //Widget Title Font Style
    $wp_customize->add_setting('spice_side_panel_title_fontstyle',
        array(
            'default'           =>  'normal',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' => 'spice_side_panel_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_side_panel_title_fontstyle', 
        array(
            'label'           => esc_html__('Font Style','spice-side-panel'),
            'section'         => 'spice_side_panel_typo_section',
            'setting'         => 'spice_side_panel_title_fontstyle',
            'type'            => 'select',
            'choices'         => $spice_side_panel_font_style,
            'active_callback' =>  'spice_side_panel_typo_callback',
        )
    );


    //Widget Title Text Transform
    $wp_customize->add_setting('spice_side_panel_title_transform',
        array(
            'default'           =>  'default',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'spice_side_panel_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_side_panel_title_transform', array(
            'label'   =>  esc_html__('Text Transform','spice-side-panel'),
            'section' => 'spice_side_panel_typo_section',
            'setting' => 'spice_side_panel_title_transform',
            'type'    => 'select',
            'choices'=>   $spice_side_panel_text_transform,
            'active_callback' =>  'spice_side_panel_typo_callback',
        )
    );



    Class Spice_Side_Panel_Content_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Widget Content', 'spice-side-panel' ); ?></h3>
        <?php }
    }

    $wp_customize->add_setting('spice_panel_content_heading',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field'
        )
    );
    $wp_customize->add_control(new Spice_Side_Panel_Content_Customize_Control($wp_customize, 'spice_panel_content_heading', 
        array(
                'active_callback'   =>  'spice_side_panel_typo_callback',
                'section'           =>  'spice_side_panel_typo_section',
                'setting'           =>  'spice_panel_content_heading'
            )
    ));


    //Widget Content Font Fmily
    $wp_customize->add_setting('spice_side_panel_content_fontfamily',
        array(
            'default'           =>  'Poppins',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'spice_side_panel_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_side_panel_content_fontfamily', 
        array(
            'label'           => esc_html__('Font Fmily','spice-side-panel'),
            'section'         => 'spice_side_panel_typo_section',
            'setting'         => 'spice_side_panel_content_fontfamily',
            'type'            => 'select',
            'choices'         =>  $spice_side_panel_font_family,
            'active_callback' => 'spice_side_panel_typo_callback',
        )
    );    


    //Widget Content Font Size
    $wp_customize->add_setting('spice_side_panel_content_fontsize',
        array(
        'default'           =>  16,
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control('spice_side_panel_content_fontsize', array(
            'label'           =>  esc_html__('Font Size (px)','spice-side-panel'),
            'section'         => 'spice_side_panel_typo_section',
            'setting'         => 'spice_side_panel_content_fontsize',
            'type'            => 'select',
            'choices'         =>  $spice_side_panel_font_size,
            'active_callback' => 'spice_side_panel_typo_callback',
        )
    );


    //Widget Content Line Height
    $wp_customize->add_setting('spice_side_panel_content_lheight',
        array(
            'default'           =>  26,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control('spice_side_panel_content_lheight', 
        array(
            'label'           =>  esc_html__('Line Height (px)','spice-side-panel'),
            'section'         => 'spice_side_panel_typo_section',
            'setting'         => 'spice_side_panel_content_lheight',
            'type'            => 'select',
            'choices'         => $spice_side_panel_line_height,
            'active_callback' => 'spice_side_panel_typo_callback',
        )
    );


    //Widget Content Font Weight
    $wp_customize->add_setting('spice_side_panel_content_fontweight',
        array(
            'default'           =>  400,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control('spice_side_panel_content_fontweight', 
        array(
        'label'           => esc_html__('Font Weight','spice-side-panel'),
        'section'         => 'spice_side_panel_typo_section',
        'setting'         => 'spice_side_panel_content_fontweight',
        'type'            =>  'select',
        'choices'         =>  $spice_side_panel_font_weight,
        'active_callback' =>  'spice_side_panel_typo_callback',
        )
    );



    //Widget Content Font Style
    $wp_customize->add_setting('spice_side_panel_content_fontstyle',
        array(
            'default'           =>  'normal',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'spice_side_panel_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_side_panel_content_fontstyle', 
        array(
        'label'          => esc_html__('Font Style','spice-side-panel'),
        'section'        => 'spice_side_panel_typo_section',
        'setting'        => 'spice_side_panel_content_fontstyle',
        'type'           => 'select',
        'choices'        =>  $spice_side_panel_font_style,
        'active_callback'=> 'spice_side_panel_typo_callback',
        )
    );


    //Widget Content Text Transform
    $wp_customize->add_setting('spice_side_panel_content_transform',
        array(
            'default'           =>  'default',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_side_panel_sanitize_text'
        )
    );
    $wp_customize->add_control('spice_side_panel_content_transform', 
        array(
            'label'           => esc_html__('Text Transform','spice-side-panel'),
            'section'         => 'spice_side_panel_typo_section',
            'setting'         => 'spice_side_panel_content_transform',
            'type'            => 'select',
            'choices'         =>  $spice_side_panel_text_transform,
            'active_callback' =>  'spice_side_panel_typo_callback',
        )
    );




    /* ===================================================================================================================
    * Side Panel Color Setting *
    ====================================================================================================================== */
    
    $wp_customize->add_section('spice_side_panel_clr_section', 
        array(
            'title'    => esc_html__('Color Settings', 'spice-side-panel' ),
            'panel'    => 'spice_side_panel',
            'priority' => 3
        )
    );
    

    $wp_customize->add_setting('enable_spice_side_panel_clr',
        array(
            'default'           => false,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'spice_side_panel_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Spice_Side_Panel_Toggle_Control( $wp_customize, 'enable_spice_side_panel_clr',
        array(
            'label'     =>  esc_html__( 'Enable to apply the settings', 'spice-side-panel'  ),
            'section'   =>  'spice_side_panel_clr_section',
            'setting'   =>  'enable_spice_side_panel_clr',
            'type'      =>  'toggle'
        )
    ));



    //Background Overlay Color
    $wp_customize->add_setting('spice_side_panel_overlay', 
        array(
            'default'           => 'rgba(0, 0, 0, .2)',
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    $wp_customize->add_control(new Spice_Side_Panel_Alpha_Color_Control($wp_customize, 'spice_side_panel_overlay', 
        array(
            'label'             =>  esc_html__('Background Overlay', 'spice-side-panel' ),
            'active_callback'   =>  'spice_side_panel_color_callback',
            'section'           =>  'spice_side_panel_clr_section',
            'setting'           =>  'spice_side_panel_overlay'
        )
    ));


    //Icon Background Color
    $wp_customize->add_setting('spice_side_panel_bg_color', 
        array(
            'default'           => '#000000',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'spice_side_panel_bg_color', 
        array(
            'label'             =>  esc_html__('Icon Background Color', 'spice-side-panel' ),
            'active_callback'   =>  'spice_side_panel_color_callback',
            'section'           =>  'spice_side_panel_clr_section',
            'setting'           =>  'spice_side_panel_bg_color'
        )
    ));


    //Icon Color
    $wp_customize->add_setting('spice_side_panel_icon_color', 
        array(
            'default'           => '#ffffff',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'spice_side_panel_icon_color', 
        array(
            'label'             =>  esc_html__('Icon Color', 'spice-side-panel' ),
            'active_callback'   =>  'spice_side_panel_color_callback',
            'section'           =>  'spice_side_panel_clr_section',
            'setting'           =>  'spice_side_panel_icon_color'
        )
    ));


    //Panel Background Color
    $wp_customize->add_setting('spice_side_panel_panel_bg_color', 
        array(
            'default'           => '#ffffff',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'spice_side_panel_panel_bg_color', 
        array(
            'label'             =>  esc_html__('Panel Background Color', 'spice-side-panel' ),
            'active_callback'   =>  'spice_side_panel_color_callback',
            'section'           =>  'spice_side_panel_clr_section',
            'setting'           =>  'spice_side_panel_panel_bg_color'
        )
    ));


    //Heading Color
    $wp_customize->add_setting('spice_side_panel_heading_color', 
        array(
            'default'           => '#222222',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'spice_side_panel_heading_color', 
        array(
            'label'             =>  esc_html__('Button/Heading Color', 'spice-side-panel' ),
            'active_callback'   =>  'spice_side_panel_color_callback',
            'section'           =>  'spice_side_panel_clr_section',
            'setting'           =>  'spice_side_panel_heading_color'
        )
    ));


    //Widget Title Color
    $wp_customize->add_setting('spice_side_panel_widget_title', 
        array(
            'default'           => '#222222',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'spice_side_panel_widget_title', 
        array(
            'label'             =>  esc_html__('Widget Title Color', 'spice-side-panel' ),
            'active_callback'   =>  'spice_side_panel_color_callback',
            'section'           =>  'spice_side_panel_clr_section',
            'setting'           =>  'spice_side_panel_widget_title'
        )
    ));


    //Widget Text/Link Color
    $wp_customize->add_setting('spice_side_panel_widget_content', 
        array(
            'default'           => '#222222',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'spice_side_panel_widget_content', 
        array(
            'label'             =>  esc_html__('Widget Text/Link Color', 'spice-side-panel' ),
            'active_callback'   =>  'spice_side_panel_color_callback',
            'section'           =>  'spice_side_panel_clr_section',
            'setting'           =>  'spice_side_panel_widget_content'
        )
    ));


    //Widget Link Hover Color
    $wp_customize->add_setting('spice_side_panel_widget_content_hover', 
        array(
            'default'           => '#222222',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'spice_side_panel_widget_content_hover', 
        array(
            'label'             =>  esc_html__('Widget Link Hover Color', 'spice-side-panel' ),
            'active_callback'   =>  'spice_side_panel_color_callback',
            'section'           =>  'spice_side_panel_clr_section',
            'setting'           =>  'spice_side_panel_widget_content_hover'
        )
    ));


}
add_action( 'customize_register', 'spice_side_panel_customizer_controls' );