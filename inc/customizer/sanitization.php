<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

// Text sanitization callback
function spice_side_panel_sanitize_text($input) {
    return wp_kses_post(force_balance_tags($input));
}


// Checkbox sanitization callbac
function spice_side_panel_sanitize_checkbox($checked) {
    // Boolean check.
    return ( ( isset($checked) && true == $checked ) ? true : false );
}


// Typography callback
function spice_side_panel_typo_callback($control) {
    if (false == $control->manager->get_setting('spice_side_panel_typo')->value()) {
        return false;
    } else {
        return true;
    }
}


// Color callback
function spice_side_panel_color_callback($control) {
    if (false == $control->manager->get_setting('enable_spice_side_panel_clr')->value()) {
        return false;
    } else {
        return true;
    }
}


//Range number sanitization callback
function spice_side_panel_sanitize_number_range($input, $setting) {

    // Ensure input is an absolute integer.
    $input = absint($input);

    // Get the input attributes associated with the setting.
    $atts = $setting->manager->get_control($setting->id)->input_attrs;

    // Get min.
    $min = ( isset($atts['min']) ? $atts['min'] : $input );

    // Get max.
    $max = ( isset($atts['max']) ? $atts['max'] : $input );

    // Get Step.
    $step = ( isset($atts['step']) ? $atts['step'] : 1 );

    // If the input is within the valid range, return it; otherwise, return the default.
    return ( $min <= $input && $input <= $max && is_int($input / $step) ? $input : $setting->default );
    
}