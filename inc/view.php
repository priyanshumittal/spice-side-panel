<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
$spice_title = get_theme_mod('spice_side_panel_title',esc_html__('Close', 'spice-side-panel' ));?>
<div class="spice-side-panel sidebar">
  <div class="panel-content">
    <div class="sp-panel-close"><i class="fa fa-times" aria-hidden="true"></i>
      <?php if($spice_title !='' ): ?><div class="spice-panel-title"> <?php echo esc_html($spice_title);?></div><?php endif;?>
    </div>
    <?php
    if ( is_active_sidebar( 'spice_panel_sidebar' ) ) :
        echo "<div class='spice_panle_wid'>"; dynamic_sidebar('spice_panel_sidebar'); echo "</div>";
    elseif ( current_user_can( 'edit_theme_options' ) ):
        echo "<div class='spice_panle_wid'><a href='".admin_url( 'widgets.php' )."'>"; esc_html_e( 'Click here to assign widget for here.', 'spice-side-panel' ); echo "</a></div>"; 
    endif; ?>
  </div>
  <div class="ssp-circle">
    <?php if(get_theme_mod('spice_side_panel_alignment','right')=='right')
    {
      echo '<i class="fa fa-chevron-right hide" aria-hidden="true"></i> <i class="fa fa-chevron-left" aria-hidden="true"></i>';
    }
    else
    {
      echo '<i class="fa fa-chevron-right" aria-hidden="true"></i><i class="fa fa-chevron-left hide" aria-hidden="true"></i>';
    }
    ?>     
  </div>
  <input type="hidden" id="spice_side_panel_id" value="<?php echo intval(get_theme_mod('spice_side_panel_width',320));?>"/>
</div>
<div class="side-panel-overlay"></div>
<style type="text/css">
  .spice-side-panel .ssp-circle
    {
      top:<?php echo intval(get_theme_mod('spice_side_panel_top_position',23))?>%;
      left:<?php echo intval(get_theme_mod('spice_side_panel_width',320));?>px;
    }
    .spice-panel-alignment-right .spice-side-panel .ssp-circle
    {
      right:<?php echo intval(get_theme_mod('spice_side_panel_width',320));?>px;
    }
  .spice-side-panel
    {
      width:<?php echo intval(get_theme_mod('spice_side_panel_width',320));?>px;
      margin-left:-<?php echo intval(get_theme_mod('spice_side_panel_width',320));?>px;
    }
    .spice-panel-alignment-right .spice-side-panel {
      margin-right:-<?php echo intval(get_theme_mod('spice_side_panel_width',320));?>px;
    }
  .spice-panel-alignment-right
    {
      right:<?php echo intval(get_theme_mod('spice_side_panel_width',260));?>px;
    }
  .spice-panel-alignment-left
    {
      left:<?php echo intval(get_theme_mod('spice_side_panel_width',320));?>px;
    }
  .spice-side-panel.sidebar .spice_panle_wid
    {
      padding-top:<?php echo intval(get_theme_mod('spice_side_panel_top',0))?>px;
      padding-right:<?php echo intval(get_theme_mod('spice_side_panel_right',0))?>px;
      padding-bottom:<?php echo intval(get_theme_mod('spice_side_panel_bottom',10))?>px;
      padding-left:<?php echo intval(get_theme_mod('spice_side_panel_left',0))?>px;  
    }
    @media (max-width:<?php echo intval(get_theme_mod('spice_side_panel_break_point',320))-1?>px){
          .spice-side-panel{display: none;}
}
</style>
<?php if(get_theme_mod('spice_side_panel_typo',false) === true):?>
<style type="text/css">
  .sp-panel-close
    {
      font-family:'<?php echo esc_attr(get_theme_mod('spice_side_panel_fontfamily','Poppins'));?>';
      font-size:<?php echo intval(get_theme_mod('spice_side_panel_fontsize',24));?>px;
      line-height:<?php echo intval(get_theme_mod('spice_side_panel_lheight',60));?>px;
      font-weight:<?php echo intval(get_theme_mod('spice_side_panel_fontweight',700));?>;
      font-style:<?php echo esc_attr(get_theme_mod('spice_side_panel_fontstyle','normal'));?>;
      text-transform:<?php echo esc_attr(get_theme_mod('spice_side_panel_transform','default'));?>;  
    }
  .spice-side-panel .widget_block h1,
  .spice-side-panel .widget_block h2,
  body .spice-side-panel .widget_block h3, body .spice-side-panel .widget-title,
  .spice-side-panel .widget_block h4,
  .spice-side-panel .widget_block h5,
  .spice-side-panel .widget_block h6
    {
      font-family:'<?php echo esc_attr(get_theme_mod('spice_side_panel_title_fontfamily','Poppins'));?>';
      font-size:<?php echo intval(get_theme_mod('spice_side_panel_title_fontsize',30));?>px;
      line-height:<?php echo intval(get_theme_mod('spice_side_panel_title_lheight',45));?>px;
      font-weight:<?php echo intval(get_theme_mod('spice_side_panel_title_fontweight',700));?>;
      font-style:<?php echo esc_attr(get_theme_mod('spice_side_panel_title_fontstyle','normal'));?>;
      text-transform:<?php echo esc_attr(get_theme_mod('spice_side_panel_title_transform','default'));?>;       
    } 
  .spice-side-panel .widget_block, .spice-side-panel .widget_block p, .spice-side-panel .widget_block a, .spice-side-panel .wp-block-calendar table caption, .spice-side-panel .wp-block-calendar table tbody, .spice-side-panel .wp-block-calendar table th, .spice-side-panel label, .spice-side-panel .wp-block-tag-cloud a, .spice-side-panel .widget_nav_menu a
    {
      font-family:'<?php echo esc_attr(get_theme_mod('spice_side_panel_content_fontfamily','Poppins'));?>';
      font-size:<?php echo intval(get_theme_mod('spice_side_panel_content_fontsize',16));?>px !important;
      line-height:<?php echo intval(get_theme_mod('spice_side_panel_content_lheight',26));?>px;
      font-weight:<?php echo intval(get_theme_mod('spice_side_panel_content_fontweight',400));?>;
      font-style:<?php echo esc_attr(get_theme_mod('spice_side_panel_content_fontstyle','normal'));?>;
      text-transform:<?php echo esc_attr(get_theme_mod('spice_side_panel_content_transform','default'));?>;       
    }  
    .spice-side-panel button, .spice-side-panel input, .spice-side-panel select, .spice-side-panel textarea
    {
      font-family:'<?php echo esc_attr(get_theme_mod('spice_side_panel_content_fontfamily','Poppins'));?>';   
    }
</style>
<?php endif;
if(get_theme_mod('enable_spice_side_panel_clr',false) === true ): ?>
<style type="text/css">
body .ssp-circle
{
  background-color:<?php echo esc_attr(get_theme_mod('spice_side_panel_bg_color','#000000'));?>;
}
body .ssp-circle
{
  color:<?php echo esc_attr(get_theme_mod('spice_side_panel_icon_color','#ffffff'));?>;
} 
body .spice-side-panel
{
  background-color:<?php echo esc_attr(get_theme_mod('spice_side_panel_panel_bg_color','#ffffff'));?>;
}
body .spice-side-panel .spice-panel-title, body .sp-panel-close .fa-times
{
  color:<?php echo esc_attr(get_theme_mod('spice_side_panel_heading_color','#222222'));?>;
}
body .spice-side-panel.sidebar .widget_block h1,
body .spice-side-panel.sidebar .widget_block h2,
body .spice-side-panel.sidebar .widget_block h3, body .spice-side-panel.sidebar .widget-title,
body .spice-side-panel.sidebar .widget_block h4,
body .spice-side-panel.sidebar .widget_block h5,
body .spice-side-panel.sidebar .widget_block h6
  {
    color: <?php echo esc_attr(get_theme_mod('spice_side_panel_widget_title','#222222'));?>
  }

body .spice-side-panel .widget_block, 
body .spice-side-panel .widget_block p, 
body .spice-side-panel .widget_block a, 
body .spice-side-panel .wp-block-calendar table caption, 
body .spice-side-panel .wp-block-calendar table tbody, 
body .spice-side-panel .wp-block-calendar table th, 
body .spice-side-panel label, 
body .spice-side-panel.sidebar .widget .wp-block-tag-cloud a, 
body .spice-side-panel .widget_nav_menu a,
body .spice-side-panel.sidebar label,
body.dark .spice-side-panel .widget_block, 
body.dark .spice-side-panel .widget_block p, 
body.dark .spice-side-panel .widget_block a, 
body.dark .spice-side-panel .wp-block-calendar table caption, 
body.dark .spice-side-panel .wp-block-calendar table tbody, 
body.dark .spice-side-panel .wp-block-calendar table th, 
body.dark .spice-side-panel label, 
body.dark .spice-side-panel.sidebar .widget .wp-block-tag-cloud a, 
body.dark .spice-side-panel .widget_nav_menu a, 
body.dark .spice-side-panel.sidebar .wp-block-calendar table tbody
{
  color: <?php echo esc_attr(get_theme_mod('spice_side_panel_widget_content','#222222'));?>
}   
body .spice-side-panel.sidebar .widget_block a:hover, 
body .spice-side-panel.sidebar .widget_block a:focus, 
body .spice-side-panel.sidebar .widget .wp-block-tag-cloud a:hover, 
body .spice-side-panel.sidebar .widget .wp-block-tag-cloud a:focus, 
body .spice-side-panel.sidebar .widget_nav_menu a:hover , 
body .spice-side-panel.sidebar .widget_nav_menu a:focus
{
  color: <?php echo esc_attr(get_theme_mod('spice_side_panel_widget_content_hover','#222222'));?>
}
body .side-panel-overlay
{
  background-color: <?php echo esc_attr(get_theme_mod('spice_side_panel_overlay','rgba(0, 0, 0, .2)'));?>
}
</style>
<?php endif;?>
